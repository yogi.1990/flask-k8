# flask-k8

Link for the code repository: <br>
    https://gitlab.com/yogi.1990/flask-k8.git

Docker hub URL for docker images. <br>
    https://hub.docker.com/repository/docker/yogi1990/flask_k8 <br>
    https://hub.docker.com/repository/docker/yogi1990/flask_with_k8 <br>

URL for Service API tier to view the records from backend tier. <br>
    http://192.168.49.2:30007 <br>
    http://192.168.49.2:30007/db <br>
    http://192.168.49.2:30007/retrieve_records <br>

Link for screen recording video showing everything mentioned above. <br>
    https://drive.google.com/file/d/1eGhd6-0Nu1LkxUZnKLbPdrwQIx7bGPNm/view?usp=drive_link