from flask import Flask
import os
import psycopg2
from dotenv import load_dotenv

load_dotenv()
app = Flask(__name__)

def db_connection():
    conn = psycopg2.connect(
        host=os.environ['POSTGRES_HOST'],
        database=os.environ['POSTGRES_DB'],
        user=os.environ['POSTGRES_USER'],
        password=os.environ['POSTGRES_PASSWORD']
    )
    cur = conn.cursor()
    return cur, conn

@app.route('/')
def home():
    return 'Hello, Flask!'

@app.route('/db')
def hello():
    try:
        cur, conn = db_connection()
        cur.execute("SELECT version()")
        db_version = cur.fetchone()
        cur.close()
        conn.close()
        return f"Postgres version: {db_version}"
    except Exception as e:
        return f"Error connecting to PostgreSQL: {str(e)}"

@app.route('/retrieve_records')
def retrieve_records():
    try:
        cur, conn = db_connection()
        cur = conn.cursor()
        cur.execute("SELECT * from test_table")
        record = cur.fetchall()
        cur.close()
        conn.close()
        return record
    except Exception as e:
        return f"Error fetching the record: {str(e)}"

if __name__ == "__main__":
    app.run(host="0.0.0.0", port=8000)
